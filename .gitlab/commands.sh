#!/bin/sh -l

# exit when any command fails; be verbose
set -ex

# make cmsrel etc. work
shopt -s expand_aliases
export MY_REL=CMSSW_5_3_32
#export MY_GT=GR_R_44_V15 GT not needed in this example
echo pwd
pwd
export MY_BUILD_DIR=${PWD}
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/cmsusr
cmsrel ${MY_REL}
cd ${MY_REL}/src

mkdir -p workspace/AOD2NanoAODOutreachTool
cd workspace/AOD2NanoAODOutreachTool
pwd
mv ${MY_BUILD_DIR}/AOD2NanoAODOutreachTool-master/* .


cmsenv
scram b -j8
#cmsRun configs/simulation_cfg.py
#change n_events before laucnhing in gitlab! The variable $1 is a gitlab CI variable and passed to this script as argument
# (a validity check for it should be added)
myline=$(grep maxEvents configs/data_cfg.py)
sed -i "s/$myline/process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32($1) )/g" configs/data_cfg.py
cmsRun configs/data_cfg.py

ls -l 

sudo cp output.root ${MY_BUILD_DIR}
ls -l ${MY_BUILD_DIR}

